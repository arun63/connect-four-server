package com.oss.connectfourserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oss.connectfourserver.domain.GameState;
import com.oss.connectfourserver.domain.Player;
import com.oss.connectfourserver.domain.UpdateBoard;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Integration tests.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTests {

    private static final String requestMapper = "/v1/api/";
    private static final String requestOriginURI = "http://127.0.0.1:";

    @Autowired
    private TestRestTemplate restTemplate;
    private RestTemplate patchRestTemplate;
    @LocalServerPort
    private int port;

    @BeforeEach
    public void setup() {
        this.patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        this.patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
    }

    @Test
    public void testStartGameAndUpdateGame() throws JsonProcessingException, JSONException {
        Player player1 = new Player();
        player1.setPlayerName("ABC");
        player1.setSymbol("X");
        Player player2 = new Player();
        player2.setPlayerName("XYZ");
        player2.setSymbol("O");
        this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player1, String.class);
        String joinPlayer2 = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player2, String.class);

        ObjectMapper mapper = new ObjectMapper();
        GameState gameState = mapper.readValue(joinPlayer2, GameState.class);

        UpdateBoard updateBoard = new UpdateBoard();
        updateBoard.setValue(3);
        updateBoard.setSymbol("X");

        JSONObject updateBody = new JSONObject();
        updateBody.put("value", 3);
        updateBody.put("symbol", "X");

        // Use patchRestTemplate to make call with PATCH method
        ResponseEntity responseEntity = patchRestTemplate.exchange(requestOriginURI + port + requestMapper +
                gameState.getGameId(), HttpMethod.PATCH, getPostRequestHeaders(updateBody.toString()), String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    public HttpEntity getPostRequestHeaders(String jsonPostBody) {
        List acceptTypes = new ArrayList();
        acceptTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON);
        reqHeaders.setAccept(acceptTypes);
        return new HttpEntity(jsonPostBody, reqHeaders);
    }
}
