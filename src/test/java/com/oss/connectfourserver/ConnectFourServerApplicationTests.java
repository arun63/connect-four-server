package com.oss.connectfourserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oss.connectfourserver.domain.GameState;
import com.oss.connectfourserver.domain.Player;
import com.oss.connectfourserver.domain.Status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ConnectFourServerApplicationTests {

    private static final String requestMapper = "/v1/api/";
    private static final String requestOriginURI = "http://127.0.0.1:";

    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;

    @Test
    void contextLoads() {
    }

    @Test
    public void testJoinGame() {
        Player player1 = new Player();
        player1.setPlayerName("ABC");
        player1.setSymbol("X");
        String joinGame = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player1, String.class);
        assertTrue(!joinGame.isEmpty());
    }

    @Test
    public void testGetGameStatus() throws JsonProcessingException {
        Player player1 = new Player();
        player1.setPlayerName("ABC");
        player1.setSymbol("X");
        String joinGame = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player1, String.class);
        ObjectMapper mapper = new ObjectMapper();
        GameState joinGameStateObject = mapper.readValue(joinGame, GameState.class);
        String expectedGameState = this.restTemplate.getForObject(requestOriginURI + port + requestMapper +
                joinGameStateObject.getGameId(), String.class);
        assertTrue(!expectedGameState.isEmpty());
    }

    @Test
    public void testConnectGameWhenTwoPlayersAreAdded() throws JsonProcessingException {
        Player player1 = new Player();
        player1.setPlayerName("ABC");
        player1.setSymbol("X");
        Player player2 = new Player();
        player2.setPlayerName("XYZ");
        player2.setSymbol("O");
        this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player1, String.class);
        String joinPlayer2 = this.restTemplate.postForObject(requestOriginURI + port + requestMapper + "join",
                player2, String.class);

        ObjectMapper mapper = new ObjectMapper();
        GameState gameState = mapper.readValue(joinPlayer2, GameState.class);
        assertEquals(gameState.status.name(), Status.TWO_PLAYER_CONNECTED.name());
    }
}
