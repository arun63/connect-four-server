package com.oss.connectfourserver.service;

import com.oss.connectfourserver.utils.GameUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Service Class Unit Tests.
 */
public class GameServiceTest {

    private static final String symbol = "[X]";
    @InjectMocks
    private GameServiceImpl gameService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testVerticalWin() {
        String[][] board = createBoard();
        board[5][5] = symbol;
        board[4][5] = symbol;
        board[3][5] = symbol;
        board[2][5] = symbol;
        board[1][5] = symbol;
        Boolean verticalWin = gameService.getVerticalWin(board, 1, 5, symbol);
        assertEquals(true, verticalWin);
    }

    @Test
    public void testHorizontalWin() {
        String[][] board = createBoard();
        board[5][4] = symbol;
        board[5][3] = symbol;
        board[5][2] = symbol;
        board[5][1] = symbol;
        board[5][0] = symbol;
        Boolean horizontalWin = gameService.getHorizontalWin(board, 5, 0, symbol);
        assertEquals(true, horizontalWin);
    }

    @Test
    public void testDiagonalLeftWin() {
        String[][] board = createBoard();
        board[5][0] = symbol;
        board[4][1] = symbol;
        board[3][2] = symbol;
        board[2][3] = symbol;
        board[1][4] = symbol;
        Boolean horizontalWin = gameService.getDiagonalLeftWin(board, 1, 4, symbol);
        assertEquals(true, horizontalWin);
    }

    @Test
    public void diagonalRightWinTest() {
        String[][] board = createBoard();
        board[5][8] = symbol;
        board[4][7] = symbol;
        board[3][6] = symbol;
        board[2][5] = symbol;
        board[1][4] = symbol;
        Boolean horizontalWin = gameService.getDiagonalRightWin(board, 1, 4, symbol);
        assertEquals(true, horizontalWin);
    }

    @Test
    public void fullColumnTest() {
        String[][] board = createBoard();
        board[5][5] = symbol;
        board[4][5] = symbol;
        board[3][5] = symbol;
        board[2][5] = symbol;
        board[1][5] = symbol;
        board[0][5] = symbol;
        int[] actualCoordinates = gameService.addGameValue(6, board, symbol);
        int[] expectedCoordinates = new int[2];
        expectedCoordinates[0] = 10;
        assertEquals(expectedCoordinates[0], actualCoordinates[0]);
    }

    @Test
    public void addValueTest() {
        String[][] board = new String[GameUtils.ROWS][GameUtils.COLUMNS];
        int[] expectedCoordinates = new int[2];
        expectedCoordinates[0] = 5;
        expectedCoordinates[1] = 4;

        int[] actualCoordinates = gameService.addGameValue(4, board, symbol);
        assertEquals(expectedCoordinates[0], actualCoordinates[0]);
        assertEquals(expectedCoordinates[1], actualCoordinates[1]);
    }

    public String[][] createBoard() {
        String[][] board = new String[GameUtils.ROWS][GameUtils.COLUMNS];
        for (int i = 0; i < GameUtils.ROWS; i++) {
            for (int z = 0; z < GameUtils.COLUMNS; z++) {
                if (board[i][z] == null) {
                    board[i][z] = "[ ]";
                }
            }
        }
        return board;
    }
}
