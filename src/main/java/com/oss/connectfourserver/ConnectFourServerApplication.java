package com.oss.connectfourserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Spring Boot Main Application Run.
 */
@SpringBootApplication
@EnableSwagger2
public class ConnectFourServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConnectFourServerApplication.class, args);
    }
}
