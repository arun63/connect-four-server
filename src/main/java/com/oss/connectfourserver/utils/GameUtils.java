package com.oss.connectfourserver.utils;

/**
 * Utility Game Service Class.
 */
public class GameUtils {

    public static final int ROWS = 6;
    public static final int COLUMNS = 9;
    public static final int WIN_COUNT = 5;
}
