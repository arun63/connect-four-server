package com.oss.connectfourserver.controller;

import com.oss.connectfourserver.domain.GameState;
import com.oss.connectfourserver.domain.Player;
import com.oss.connectfourserver.domain.UpdateBoard;
import com.oss.connectfourserver.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Application Controller.
 * We define end point POST, GET, PATCH to set and get the game.
 */
@RestController
@RequestMapping("v1/api")
public class ApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiController.class);

    private final GameService gameService;

    public ApiController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/join", method = RequestMethod.POST, consumes = {"application/json"})
    public GameState createGame(@RequestBody Player newPlayer) {
        LOGGER.info("Add new player to the game - {} and his symbol: {}", newPlayer.getPlayerName(),
                newPlayer.getSymbol());

        new GameState();
        GameState gameState;
        gameState = gameService.addNewPlayer(newPlayer);
        return gameState;
    }

    @RequestMapping(value = "/{gameId}", method = RequestMethod.GET)
    public GameState getGameStatus(@PathVariable UUID gameId) {
        LOGGER.info("Retrieving the game state for id - {} ", gameId);
        new GameState();
        GameState gameState;
        gameState = gameService.getGameStatus(gameId);
        return gameState;
    }

    @RequestMapping(value = "/{gameId}", method = RequestMethod.PATCH, consumes = {"application/json"})
    public GameState updateGame(@PathVariable UUID gameId, @RequestBody UpdateBoard updateBoard) {
        LOGGER.info("Updating the game state for id - {} and updateValue - {}", gameId, updateBoard.getValue());
        new GameState();
        GameState gameState;
        gameState = gameService.updateGameBoard(gameId, updateBoard);
        return gameState;
    }
}
