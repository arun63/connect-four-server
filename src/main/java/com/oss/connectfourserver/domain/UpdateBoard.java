package com.oss.connectfourserver.domain;

import java.io.Serializable;

/**
 * The domain model to update the game state.
 */
public class UpdateBoard implements Serializable {

    private int value;
    private String symbol;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
