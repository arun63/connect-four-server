package com.oss.connectfourserver.domain;

/**
 * Holds the status of the game.
 */
public enum Status {
    ONE_PLAYER_CONNECTED, TWO_PLAYER_CONNECTED, COMPLETED, COLUMN_EXISTS
}
