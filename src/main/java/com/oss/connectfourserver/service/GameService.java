package com.oss.connectfourserver.service;

import com.oss.connectfourserver.domain.GameState;
import com.oss.connectfourserver.domain.Player;
import com.oss.connectfourserver.domain.UpdateBoard;

import java.util.UUID;

/**
 * Interface for game service implementation.
 */
public interface GameService {

    /**
     * Adding new player to the game.
     *
     * @param newPlayer player object.
     * @return the game state object.
     */
    GameState addNewPlayer(Player newPlayer);

    /**
     * Get the game status for the given unique game identifier.
     *
     * @param gameId the unique game identifier.
     * @return the game state object.
     */
    GameState getGameStatus(UUID gameId);

    /**
     * Update the game board based on its new value.
     *
     * @param gameId      game identifier.
     * @param updateBoard the object containing the value and the symbol.
     * @return the game state object.
     */
    GameState updateGameBoard(UUID gameId, UpdateBoard updateBoard);
}
