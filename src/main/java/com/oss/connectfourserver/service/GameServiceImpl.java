package com.oss.connectfourserver.service;

import com.oss.connectfourserver.domain.GameState;
import com.oss.connectfourserver.domain.Player;
import com.oss.connectfourserver.domain.Status;
import com.oss.connectfourserver.domain.UpdateBoard;
import com.oss.connectfourserver.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * The Game Service Implementation.
 */
@Service
public class GameServiceImpl implements GameService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameServiceImpl.class);

    private static final HashMap<UUID, GameState> gameStateHashMap = new HashMap<>();

    public GameState addNewPlayer(Player newPlayer) {
        GameState game = new GameState();
        String[] symbols = new String[2];
        for (Map.Entry<UUID, GameState> entry : gameStateHashMap.entrySet()) {
            GameState gameState = entry.getValue();
            if (gameState.getPlayer2() == null) {
                gameState.setPlayer2(newPlayer);
                gameState.setStatus(Status.TWO_PLAYER_CONNECTED);
                gameState.player2.setPlayerName(newPlayer.getPlayerName());
                gameState.player2.setSymbol(newPlayer.getSymbol());
                gameState.setValue(gameState.getPlayer1().getSymbol());
                symbols[0] = gameState.getPlayer1().getSymbol();
                symbols[1] = newPlayer.getSymbol();
                gameState.setSymbols(symbols);
                return gameState;
            }
        }
        createNewGame(newPlayer.getPlayerName(), newPlayer.getSymbol(), game);
        return game;
    }

    public void createNewGame(String playerId, String symbol, GameState game) {
        String[][] board = new String[GameUtils.ROWS][GameUtils.COLUMNS];
        Player player = new Player();
        player.setPlayerName(playerId);
        player.setSymbol(symbol);
        game.setGameId(UUID.randomUUID());
        game.setPlayer1(player);
        game.setStatus(Status.ONE_PLAYER_CONNECTED);
        game.setBoard(board);

        gameStateHashMap.put(game.gameId, game);
    }

    public GameState getGameStatus(UUID gameId) {
        GameState game = new GameState();
        for (Map.Entry<UUID, GameState> entry : gameStateHashMap.entrySet()) {
            UUID key = entry.getKey();
            GameState currentGame = entry.getValue();
            if (key.equals(gameId)) {
                return currentGame;
            }
        }
        return game;
    }

    public GameState updateGameBoard(UUID gameId, UpdateBoard updateBoard) {
        GameState game = getGameStatus(gameId);
        int[] coordinates = addGameValue(updateBoard.getValue() - 1, game.getBoard(), updateBoard.getSymbol());
        if (coordinates[0] != 10) {
            for (Map.Entry<UUID, GameState> entry : gameStateHashMap.entrySet()) {
                UUID key = entry.getKey();
                GameState currentGame = entry.getValue();
                if (key.equals(gameId)) {
                    currentGame.setBoard(game.getBoard());
                }
            }
        } else {
            game.setStatus(Status.COLUMN_EXISTS);
        }

        Boolean winStatus = getWinStatus(game.getBoard(), coordinates, updateBoard.getSymbol());
        if (winStatus.equals(true)) {
            game.setStatus(Status.COMPLETED);
            game.setWinner(game.getValue());
        } else {
            updateGame(game);
        }
        return game;
    }

    public void updateGame(GameState game) {
        String[] gameSymbols = game.getSymbols();
        if (gameSymbols[0].equals(game.getValue())) {
            game.setValue(gameSymbols[1]);
        } else if (gameSymbols[1].equals(game.getValue())) {
            game.setValue(gameSymbols[0]);
        }
    }

    public int[] addGameValue(int value, String[][] board, String symbol) {
        int[] coordinates = new int[2];
        if (board[0][value] != null) {
            coordinates[0] = 10;
            return coordinates;
        }
        for (int i = GameUtils.ROWS - 1; i >= 0; i--) {
            if (board[i][value] == null) {
                board[i][value] = "[" + symbol + "]";
                coordinates[0] = i;
                coordinates[1] = value;
                return coordinates;
            }
        }
        return coordinates;
    }

    public Boolean getWinStatus(String[][] board, int[] coordinates, String symbols) {
        String symbolValue = "[" + symbols + "]";
        int row = coordinates[0];
        int col = coordinates[1];
        return getVerticalWin(board, row, col, symbolValue) ||
                getHorizontalWin(board, row, col, symbolValue) ||
                getDiagonalLeftWin(board, row, col, symbolValue) ||
                getDiagonalRightWin(board, row, col, symbolValue);
    }

    public Boolean getVerticalWin(String[][] board, int row, int col, String symbolValue) {
        int verticalCount = 1;

        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row + i][col].equals(symbolValue)) {
                    verticalCount++;
                } else {
                    break;
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        return verticalCount == GameUtils.WIN_COUNT;
    }

    public Boolean getHorizontalWin(String[][] board, int row, int col, String symbolValue) {
        int horizontalCount = 1;
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row][col + i] != null && board[row][col + i].equals(symbolValue)) {
                    horizontalCount++;
                } else {
                    break;
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        if (horizontalCount == GameUtils.WIN_COUNT) {
            return true;
        }
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row][col - i] != null && board[row][col - i].equals(symbolValue)) {
                    horizontalCount++;
                    if (horizontalCount == GameUtils.WIN_COUNT) {
                        return true;
                    }
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        return false;
    }

    public Boolean getDiagonalLeftWin(String[][] board, int row, int col, String symbolValue) {
        int diagonalLeftCount = 1;
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row - i][col + i] != null && board[row - i][col + i].equals(symbolValue)) {
                    diagonalLeftCount++;
                } else {
                    break;
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        if (diagonalLeftCount == GameUtils.WIN_COUNT) {
            return true;
        }
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row + i][col - i] != null && board[row + i][col - i].equals(symbolValue)) {
                    diagonalLeftCount++;
                    if (diagonalLeftCount == GameUtils.WIN_COUNT) {
                        return true;
                    }
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }

        return false;
    }

    public Boolean getDiagonalRightWin(String[][] board, int row, int col, String symbolValue) {
        int diagonalRightCount = 1;
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row + i][col + i] != null && board[row + i][col + i].equals(symbolValue)) {
                    diagonalRightCount++;
                } else {
                    break;
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        if (diagonalRightCount == GameUtils.WIN_COUNT) {
            return true;
        }
        for (int i = 1; i <= GameUtils.WIN_COUNT; i++) {
            try {
                if (board[row - i][col - i] != null && board[row - i][col - i].equals(symbolValue)) {
                    diagonalRightCount++;
                    if (diagonalRightCount == GameUtils.WIN_COUNT) {
                        return true;
                    }
                }
            } catch (IndexOutOfBoundsException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
        return false;
    }
}
