[![build status](https://gitlab.com/arun63/connect-four-server/badges/development/pipeline.svg)](https://gitlab.com/arun63/connect-four-server})
[![coverage report](https://gitlab.com/arun63/connect-four-server/badges/development/coverage.svg)](https://gitlab.com/arun63/connect-four-server/-/commits/development)

# Connect Four Server

> A simple and basic famous connect four game (5-in-a-row) microservices that provides end points for the two player connection game. 
The service is containerized using docker and deployed using the kubernetes cluster from its helm chart.

---
### Tech Stack

- [Java 11](https://www.oracle.com/ie/java/technologies/javase-jdk11-downloads.html)
- [Spring boot](https://spring.io/blog/2021/01/14/spring-boot-2-4-2-available-now)
- [JUnit](https://junit.org/junit5/docs/current/user-guide/)
- [Swagger](https://swagger.io/)
- [Docker](https://www.docker.com/)
- [Kubernetes](https://kubernetes.io/)
- [Helm](https://helm.sh/)
- [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) 
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)

---

### Commands
- #### local installation
  For Building and Running the spring boot application
  ```sh
  gradle build && gradle bootRun
  ```
- #### Docker 
  Alternatively, use docker for building and running the application
  ```sh
  docker build -t connect-four-server .
  ```
  To run the docker image
  ```sh
  docker run -p 8080:8080 connect-four-server
  ```
  
### Sample Usage
- #### Curl or Postman
```sh
# Add new player and starting the game.
curl -X POST "http://localhost:8080/v1/api/join" \
    -H "Content-Type: application/json" \
    -d "{ \"playerName\": \"Arun\", \"symbol\": \"X\"}"
    
# returns Json string with unique identifier - gameId.

# To get the status of the game. 
curl -X GET "http://localhost:8080/v1/api/<GAME_ID>" -H "Content-Type: application/json" 

```
- #### Alternatively, use the Swagger UI Endpoint
  ```sh
  http://localhost:8080/swagger-ui.html
  ```

### Continuous Integration

3 stages of pipelines on each commit
```
 - compile  # compiles - gradle compile
 - test     # run unit and functional tests - gradle check
 - qa       # code quality analysis - it run the checkstyle, jacoco report and jacoco verificaton
 - build-container # docker containization for the deployment
```
The docker container is pushed into the docker registry that will be access for pull the images in the kubernetes cluster.

### Deployment using Helm Chart 

- ### Running it on minikube

Create a namespace
```sh
export NAMESPACE="connect-four-server"

envsubst < infra/namespace.yaml | kubectl apply -f -
```

Using the helm to deploy into the kubernetes clusters
```sh
helm upgrade url-shortener-service infra/service-app --install --namespace=${NAMESPACE}
```

The default configuration is set to min `cpu=125m, memory=256Mi` and max `cpu=500m, memory=1024Mi`.

The Application is `statefulsets` and have horizontal scale up to `5` nodes when the `CPU Utilization is > 90%`

On success deployment using helm chart would look as below.
```markdown
Release "connect-four-server" does not exist. Installing it now.
NAME: connect-four-server
LAST DEPLOYED: Tue Mar  9 09:38:29 2021
NAMESPACE: connect-four-server
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

To check all the pods are ready and running.

```markdown
kubectl get pods -n connect-four-server                    
NAME                    READY   STATUS    RESTARTS   AGE
connect-four-server-0  1/1    Running   0          1m 19s
```

To stop the cluster and delete the namespaces. 

```sh
envsubst < infra/namespace.yaml | kubectl delete -f -
```

- ### Running it on GKE 

Create the cluster and namespaces
```sh
# Get the key from the GKE console and name it as account.json
export GOOGLE_APPLICATION_CREDENTIALS=account.json

gcloud auth activate-service-account --key-file account.json

# Define your project_id, region and name
gcloud --project=${GKE_PROJECT_ID} container clusters get-credentials --region=${GKE_REGION} ${GKE_CLUSTER_NAME}

```
Follow the same steps as deployment on minikube for create namespaces and deployment of the application using the helm chart.


### Approach
- Microservices Architecture (Spring boot) is termed as stand-alone, production-grade hierarchical structure.
- Java 11 as the preferred language and junit as the test framework. 
- Swagger as the schema representation and documentation. 
- The application runs on the public and private cloud infrastructure supporting horizontal scaling
- Docker is used as the preferred containerization and the gitlab for the ci/cd infrastructure 
- Helm Chart is used as deployment versioning into the kubernetes cluster.
- Test Coverage is `>85%`

### Improvements
- Add GKE into `.gitlab.yml` for continuous delivery
- Add cert-manager and load balancer support 
- Using Grafana and Prometheus Metrics to monitor the clusters
- Introducing reactive streams such as Akka for avoid long pulling
- Persistence storage or in-memory database for data backup

### Branch

Creating a new branch based on the feature, bugfix, hotfix, security
- `feature/<feature-name>` - The product incremental feature
- `bugfix/<bugfix-name>` - The bugfix involved in various stages and feature
- `hotfix/<hotfix-name>` - Immediate fix that requires immediate merge
- `security/<security-name>` - security related features and fixes.

`Note`: Pipeline will only trigger if it follows branching strategy