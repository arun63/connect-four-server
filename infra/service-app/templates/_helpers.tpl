{{- define "service.labels" -}}
helm_release: {{ .Release.Name }}
helm_chart_name: {{ .Chart.Name }}
helm_app_version: {{ .Chart.Version }}
domain: {{ .Values.domain.name }}
domain_app: {{ .Values.domain.app_name }}
{{- end -}}
