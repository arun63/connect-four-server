FROM adoptopenjdk/openjdk11:alpine-jre

RUN addgroup -S microservice && adduser -S microservice -G microservice
USER microservice:microservice
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]